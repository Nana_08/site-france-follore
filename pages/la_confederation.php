<link rel="stylesheet" href="../public/css/organigramme.css">
<section>
    <?php require("templates/template_navbar.php"); ?>
    <h1 id="title_page">La confédération</h1>
</section>

<section id="confederation">
    <img src="../public/img/la_confederation.png" alt="photo de la confédération">
    <h2>Présentation de la confédération</h2>
    <p>La Confédération française des arts et traditions populaires rassemble, par le biais de ses 10 fédérations, des associations œuvrant pour la valorisation et la transmission des arts et traditions populaires. Elle rassemble 82 groupes à travers toute la France.
        Elle veille à donner du sens à la culture populaire par la recherche, la connaissance et la transmission.
        Elle contribue à l’éducation populaire et à la formation dans une exigence de qualité
        Elle s’engage à respecter et exprimer la mémoire du patrimoine immatériel et matériel.
        Elle réalise des recherches dans le domaine des arts populaires tel que langues dialectes, chants costumes musiques, contes, us et coutumes, matériels et outils : en général de tous les éléments du patrimoine du ou des terroirs aux périodes représentées pour restituer ce patrimoine le plus fidèlement possible dans un souci pédagogique, éducatif ou de façon purement artistique.
    </p>
    <div id="buttons">
        <button class="btn" id="bureaux">Bureaux</button>
        <button class="btn" id="commission">Commission</button>
        <button class="btn" id="federation">Fédérations</button>
        <button class="btn" id="IGF">IGF</button>
    </div>


    <article id="content_bureaux" class="content">
        <h2>Organigramme</h2>
        <figure class="org-chart cf">
            <ul class="administration">
                <li>
                    <ul class="director">
                        <li>
                            <a href="#"><span>M CHARLETY Nicolas<br>Président</span></a>
                            <ul class="departments cf">
                                <li><a href="#"><span>Bureau</span></a></li>

                                <li class="department dep-a">
                                    <a href="#"><span>Vice Présidence</span></a>
                                    <ul class="sections">
                                        <li class="section"><a href="#"><span>Mme CHASSARD Sylvia<br>Vice-présidente</span></a></li>
                                        <li class="section"><a href="#"><span>M PERIGAUD Bernard<br>Vice-président</span></a></li>
                                        <li class="section"><a href="#"><span>M CLUZEAUD Frédéric<br>Vice-président</span></a></li>
                                    </ul>
                                </li>
                                <li class="department dep-b">
                                    <a href="#"><span>Trésorie</span></a>
                                    <ul class="sections">
                                        <li class="section"><a href="#"><span>M RIFAULT André-Patrice<br>Trésorier</span></a></li>
                                        <li class="section"><a href="#"><span>M BOBLET Michel<br>Trésorier-adjoint</span></a></li>
                                    </ul>
                                </li>
                                <li class="department dep-c">
                                    <a href="#"><span>Secrétariat</span></a>
                                    <ul class="sections">
                                        <li class="section"><a href="#"><span>Mme CHARREAU Marie-Agnès<br>Secrétaire</span></a></li>
                                        <li class="section"><a href="#"><span>Mme THEOBALD Sylvie<br>Secrétaire-adjointe</span></a></li>
                                    </ul>
                                </li>

                            </ul>

                            <ul class="subdirector">
                                <li><a href="#"><span>Conseil</span></a></li>
                                <li class="department dep-d">
                                    <a href="#"><span>Membres</span></a>
                                    <ul class="sections">
                                        <li class="section"><a href="#"><span>Mme ANTOINE Christiane</span></a></li>
                                        <li class="section"><a href="#"><span>Mr ARNAUD Dominique</span></a></li>
                                        <li class="section"><a href="#"><span>Mme BASCAULES Brigitte</span></a></li>
                                        <li class="section"><a href="#"><span>Mme BOUSSELY Nelly</span></a></li>
                                        <li class="section"><a href="#"><span>Mr CHASSARD Joël</span></a></li>
                                        <li class="section"><a href="#"><span>Mme FAVARD Marion</span></a></li>
                                        <li class="section"><a href="#"><span>Mr FEYBESSE Jean-Claude</span></a></li>
                                        <li class="section"><a href="#"><span>Mlle GIORDANENGES Lise</span></a></li>
                                        <li class="section"><a href="#"><span>Mr GUICHET Michel</span></a></li>
                                        <li class="section"><a href="#"><span>Mr MAUCHERAT Thierry</span></a></li>
                                    </ul>
                                </li>
                                <li class="department dep-e">
                                    <a href="#"><span>Membres</span></a>
                                    <ul class="sections">
                                        <li class="section"><a href="#"><span>Mr PINS Marcel</span></a></li>
                                        <li class="section"><a href="#"><span>Mme MOUSSLER Julie</span></a></li>
                                        <li class="section"><a href="#"><span>Mr QUINQUINET Gérard</span></a></li>
                                        <li class="section"><a href="#"><span>Mme RICHARD Gisèle</span></a></li>
                                        <li class="section"><a href="#"><span>Mr SARRAIL Claude</span></a></li>
                                        <li class="section"><a href="#"><span>Mme SMIGIELSKI Caroline</span></a></li>
                                        <li class="section"><a href="#"><span>M SOURISSEAU René</span></a></li>
                                        <li class="section"><a href="#"><span>Mme TONNELIER Claude</span></a></li>
                                        <li class="section"><a href="#"><span>Mlle TOURTET Hélène</span></a></li>
                                        <li class="section"><a href="#"><span>Mme WEBER Georgette</span></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </figure>
    </article>

    <article id="content_commission" class="content">
        <?php
        require('templates/template_commission.php');
        ?>
    </article>

    <article id="content_federation" class="content">
        <?php
        require('templates/template_federation.php');
        ?>
    </article>

    <article id="content_IGF" class="content">
        <?php
        require('templates/template_IGF.php');
        ?>
    </article>





    <div id="contact">
        <h2>Nous contacter</h2>
        <a href="index.php?page=groupes"><button class="btn" id="groupes">Les groupes de la confédération</button></a>
    </div>

</section>



<script type="text/javascript">
    let buttons = document.querySelectorAll("#buttons>button");
    buttons.forEach(function(elem, index) {
        elem.addEventListener('click', function() {
            removeClasses();
            this.classList.add('active');
            let articles = document.querySelectorAll("article.content:not(#content_" + this.id + ")");
            displayOnly(articles);
            switch (this.id) {
                case 'bureaux':
                    document.getElementById('content_bureaux').style.display = 'block';
                    break;
                case 'commission':
                    document.getElementById('content_commission').style.display = 'flex';
                    break;
                case 'federation':
                    document.getElementById('content_federation').style.display = 'flex';
                    break;
                case 'IGF':
                    document.getElementById('content_IGF').style.display = 'flex';
                    break;
            }
        });
    });

    function removeClasses() {
        buttons.forEach(function(elem, index) {
            elem.classList.remove('active');
        })
    };

    function displayOnly(articles) {
        articles.forEach(function(elem) {
            elem.style.display = 'none';
        })
    }
</script>