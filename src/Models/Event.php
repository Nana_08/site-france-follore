<?php

class Event{

    private $id;
    private $nom;
    private $date;
    private $localisation;
    private $departement;
    private $description; 
    private $organisateur;

    public function __construct($id, $nom, $date, $localisation, $departement, $description, $organisateur)
    {
        $this->id = $id;
        $this->nom = $nom;
        $date = new DateTime($date);
        $this->date = $date->format('d-m-Y');
        $this->localisation = $localisation;
        $this->departement = $departement;
        $this->description = $description;
        $this->organisateur = $organisateur;    
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * @param mixed $localisation
     */
    public function setLocalisation($localisation): void
    {
        $this->localisation = $localisation;
    }

    /**
     * @return mixed
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * @param mixed $departement
     */
    public function setDepartement($departement): void
    {
        $this->departement = $departement;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getOrganisateur()
    {
        return $this->organisateur;
    }

    /**
     * @param mixed $organisateur
     */
    public function setOrganisateur($organisateur): void
    {
        $this->organisateur = $organisateur;
    }

    public function toArray(){
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'date' => $this->date,
            'description' => $this->description,
            'organisateur' => $this->organisateur,
            'departement' => $this->departement,
            'localisation' => $this->localisation
        ];
    }
}


?>