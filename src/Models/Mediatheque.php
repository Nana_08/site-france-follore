<?php

class Mediatheque
{
    private $id;
    private $type;
    private $titre;
    private $description;
    private $lienMedia;

    /**
     * Mediatheque constructor.
     * @param $id
     * @param $type
     * @param $titre
     * @param $description
     * @param $lienMedia
     */
    public function __construct($id, $type, $titre, $description, $lienMedia)
    {
        $this->id = $id;
        $this->type = $type;
        $this->titre = $titre;
        $this->description = $description;
        $this->lienMedia = $lienMedia;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre): void
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLienMedia()
    {
        return $this->lienMedia;
    }

    /**
     * @param mixed $lienMedia
     */
    public function setLienMedia($lienMedia): void
    {
        $this->lienMedia = $lienMedia;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'titre' => $this->getTitre(),
            'description' => $this->getDescription(),
            'lienMedia' => $this->getLienMedia(),
        ];
    }
}
