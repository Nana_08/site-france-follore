DROP TABLE IF EXISTS `groupes`;

CREATE TABLE `groupes` (
                               `id` int(11) NOT NULL AUTO_INCREMENT,
                               `nom` varchar(250) NOT NULL,
                               `description` text NOT NULL,
                               `dpt` int(11) NOT NULL,
                               `reseaux` varchar(250) NULL,
                               `site` varchar(250) NOT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;