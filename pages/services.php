<section>
    <?php require("templates/template_navbar.php"); ?>
    <h1 id="title_page">Les services</h1>
    <img class="fleche_back" src="public/img/svg/arrow.svg" alt="flèche de retour" style="transform: rotate(-90deg)">
    <section class="services_container" id="services">


    </section>
</section>

<?php require("templates/template_services.html"); ?>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        let m = {
            data: undefined,

            init: function () {
                this.data = <?php echo json_encode($services); ?>;
                console.log(m.data)
            },
        }

        let c = {
            init: function () {
                m.init();
                v.init();
            },
        }

        let v = {
            container: undefined,
            parentItems: undefined,
            back: undefined,

            init: function () {
                this.back = document.querySelector(".fleche_back");
                this.renderItems();
            },

            renderItems: function () {
                v.back.style.display = "none";
                v.parentItems = document.querySelector(".services_container");
                v.parentItems.innerHTML = "";

                for (let i = 1; i < Object.keys(m.data).length; i++) {
                    if (m.data[i] != undefined) {
                        let content = document.getElementById("template_services").content;
                        let newItem = document.importNode(content, true);
                        newItem.querySelector(".the_service h2").textContent = m.data[i][i - 1][1];
                        newItem.querySelector(".the_service p").textContent = m.data[i][i - 1][2];
                        newItem.querySelector(".the_service button").addEventListener("click", v.renderSubItems);
                        newItem.querySelector(".the_service").dataset.id = i;
                        v.parentItems.appendChild(newItem);
                    }
                }
                ;
            },

            renderSubItems: function (ev) {
                v.back.style.display = "block";
                v.back.addEventListener("click", v.renderItems)
                v.parentItems.textContent = ""
                let key = ev.path[1].dataset.id

                for (let i = 1; i < Object.keys(m.data).length; i++) {
                    if (m.data["sub" + i] != undefined) {
                        let content = document.getElementById("template_services").content;
                        let newItem = document.importNode(content, true);
                        newItem.querySelector(".the_service h2").textContent = m.data["sub" + key][i - 1].titre;
                        newItem.querySelector(".the_service p").textContent = m.data["sub" + key][i - 1].description;
                        newItem.querySelector(".the_service button").style.display = "none";
                        v.parentItems.appendChild(newItem);
                    }
                }


            }


        }

        c.init();
    })
</script>