function formatDate(date) {
    return date.split('-').reverse().join('-');
}

function groupPerPage(items) {
    let groupped = [];

    let counter = 0;
    items.forEach(function(elem, index) {
        if (index % 5 === 0) {
            counter++;
            groupped[counter] = [];
        }

        groupped[counter].push(elem);
    });
    return groupped;
}

/** On ajoute les actions à chaque ligne **/
function addActions(tr, article) {
    let tdActions = document.createElement("td");
    let icons = ["trash-alt", "edit"];

    icons.forEach(function(elem) {
        let icon = document.createElement("i");
        icon.classList.add('icon');
        icon.classList.add('fa');
        icon.classList.add(`fa-${elem}`);
        tdActions.appendChild(icon);
    });

    tdActions.dataset.id = article.id;
    tr.appendChild(tdActions);
}

function closeModalOutsideClick(modal) {
    window.addEventListener('click', function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    });
}

function initPager(items, functionInit) {
    let keysPage = Object.keys(groupPerPage(items))

    if (keysPage.length > 1) {
        keysPage.forEach(function(elem) {
            let numPager = document.createElement('div');
            numPager.innerText = elem;
            numPager.classList.add('num-pager');
            if (parseInt(elem) === page) {
                numPager.classList.add('active');
            }
            numPager.addEventListener('click', function(e) {
                page = this.innerText;
                functionInit();
                removeClassesPager();
                this.classList.add('active');

            })
            pager.appendChild(numPager);
        });
    }
}

function removeClassesPager() {
    let buttonsPager = document.querySelectorAll('.num-pager');
    buttonsPager.forEach(function(elem, index) {
        elem.classList.remove('active');
    })
}