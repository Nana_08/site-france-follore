<section id="accueil_section">
    <?php require("templates/template_navbar.php"); ?>
    <h1>Association France Folklore</h1>
</section>

<section id="articles">
    <?php

    /** Pour chaque articles, on appel le template HTML d'article */
    foreach($articles as $article)
    {
        require('templates/template_article.php');
    }
    ?>
</section>
