let buttonArticle = document.getElementById("article");
let columnArticles = ["Nom de l'article", "Date de l'article", "Contenu de l'article", "Actions"]

buttonArticle.addEventListener('click', function() {
    pager.innerHTML = "";
    page = 1;

    initArticles();
    initPager(articles, initArticles);

    closeModalOutsideClick(modal)
});

function initArticles() {
    let thead = document.querySelector("thead>tr");
    thead.innerHTML = "";
    let tbody = document.querySelector("tbody");
    tbody.innerHTML = "";
    let modal = document.getElementById('modal');

    /** On ajoute les colonnes dans le DOM **/
    columnArticles.forEach(function(elem) {
        let th = document.createElement("th");
        th.innerText = elem;
        th.classList.add('col-3');
        thead.appendChild(th);
    });

    /** Pour chaque articles, on ajoute les datas associés **/
    groupPerPage(articles)[page].forEach(function(article) {
        let tr = document.createElement("tr");
        let articleCopy = {...article };
        delete articleCopy.id;
        delete articleCopy.media;
        delete articleCopy.type;

        let keys = Object.keys(articleCopy);

        keys.forEach(function(elem) {
            let td = document.createElement("td");
            td.innerText = article[elem];
            tr.appendChild(td);
        });

        addActions(tr, article);
        tbody.appendChild(tr);
    })

    /** Pour chaque icon, on rajoute un event listener "onclick", permet de faire une action **/
    let icons = document.querySelectorAll("td>i.icon");
    icons.forEach(function(elem) {
        elem.addEventListener("click", function(e) {
            let id = parseInt(e.currentTarget.parentNode.dataset.id);
            /** On récupère l'article que l'on modifie **/
            let currentArticle = articles.find(function(elem) {
                if (elem.id === id) {
                    return elem;
                }
            });

            if (elem.classList.contains("fa-edit")) {
                currentArticle.date = formatDate(currentArticle.date);
                let modalContentEdit = document.querySelector('#modal>.modal-content');
                /** On ajoute le contenu de la modal **/
                modalContentEdit.innerHTML = `
                        <span class="close">&times;</span>
                        <h3 class="modal-title">Edition de l'article</h3>
                        <form method="POST" action="index.php?page=backoffice&mode=edit" >
                            <div class="form-group">
                                <label class="col-4" for="nom">Nom de l'article :</label>
                                <input type="text" class="col-6" placeholder="Nom de l'article..." id="nom" name="nom" value="` + currentArticle.nom + `"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="date">Date de l'article :</label>
                                <input type="date" class="col-6" placeholder="Date de l'article..." id="date" name="date" value="` + currentArticle.date + `"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="date">Description de l'article :</label>
                                <textarea class="col-6" placeholder="Description de l'article..." id="description" name="description">` + currentArticle.description + `</textarea>
                            </div>
                            <input type="hidden" value="` + currentArticle.id + `" name="id"/>
                            <input type="hidden" value="article" name="type"/>
                            <button type="submit" class="btn">Editer</button>
                        </form>`;
            } else if (elem.classList.contains("fa-trash-alt")) {
                let modalContentDelete = document.querySelector('#modal>.modal-content');
                modalContentDelete.innerHTML = `
                    <span class="close">&times;</span>
                    <h3 class="modal-title">Suppression de l'article</h3>
                    <p>Êtes vous sur de vouloir supprimer l'article " ` + currentArticle.nom + ` " ?</p>
                    <form method="POST" action="index.php?page=backoffice&mode=delete">
                        <input type="hidden" value="` + currentArticle.id + `" name="id"/>
                        <input type="hidden" value="article" name="type"/>
                        <button type="submit" class="btn">Supprimer</button>
                    </form>
                `;
            }
            modal.style.display = "block";
            /** Permet de fermer la modal au clique sur la croix **/
            var span = document.querySelector("span.close");

            span.onclick = function() {
                modal.style.display = "none";
            }
        })
    });
}

function initModalCreateArticle() {
    let modal = document.getElementById('modal');
    let modalContentCreate = document.querySelector('#modal>.modal-content');

    modalContentCreate.innerHTML = `
        <span class="close">&times;</span>
        <h3 class="modal-title">Création d'un article</h3>
        <form method="POST" action="index.php?page=backoffice&mode=create" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-4" for="nom">Nom de l'article :</label>
                <input type="text" class="col-6" placeholder="Nom de l'article..." id="nom" name="nom" />
            </div>
            <div class="form-group">
                <label class="col-4" for="date">Date de l'article :</label>
                <input type="date" class="col-6" placeholder="Date de l'article..." id="date" name="date" />
            </div>
            <div class="form-group">
                <label class="col-4" for="description">Description de l'article :</label>
                <textarea class="col-6" placeholder="Description de l'article..." id="description" name="description"></textarea>
            </div>
            <div class="form-group">
                <label class="col-4" for="media">Image de l'article :</label>
                <input type="file" name="media" id="media" class="col-6"/>
            </div>
            <input type="hidden" value="article" name="type"/>
            <button type="submit" class="btn">Créer</button>
        </form>
    `;
    modal.style.display = "block";

    var span = document.querySelector("span.close");
    span.onclick = function() {
        modal.style.display = "none";
    }

    closeModalOutsideClick(modal)
}