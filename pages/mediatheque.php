<?php
function jsonSerialize($article)
{
    return array(
        'type' => $article->getType(),
        'titre' => $article->getTitre(),
        'description' => $article->getDescription(),
        'lien' => $article->getLienMedia(),
    );
}

$data = [];

foreach ($mediatheque as $article) {
    array_push($data, jsonSerialize($article));
}

?>


<section id="page-mediatheque">
    <?php require("templates/template_navbar.php"); ?>
    <h1 id="title_page">La médiathèque</h1>

    <section class="buttons">
        <div class="col-8 container">
            <button class="btn active" data-id="video">Vidéos</button>
            <button class="btn" data-id="musique">Musiques</button>

        </div>
    </section>
    <section class="medias">

    </section>
</section>


<section>

</section>



<?php require("templates/template_mediatheque.html"); ?>

<script>
    //document.addEventListener('DOMContentLoaded', function() {
    let m = {
        category: "video",
        boutique: undefined,
        items: [],

        init: function() {
            this.boutique = <?php echo json_encode($data); ?>;
        }
    }

    let c = {
        init: function() {
            m.init();
            v.init();
        },

        filterCategories: function(e) {
            v.clearItems();
            v.renderItems(e.target.textContent);
        },

        setItemsByCategory: function() {

            m.items = m.boutique.filter(function(elem) {
                if (m.category.toLowerCase() === elem.type.toLowerCase()) {
                    return elem;
                }
            });

            v.renderItems();
        }
    }

    let v = {
        buttons: [],

        init: function() {
            this.initButtons();
            this.renderItems(c.setItemsByCategory());
        },

        renderItems: function() {
            let parentItems = document.querySelector(".medias");
            parentItems.innerHTML = "";
            m.items.forEach(function(elem) {
                let content = document.getElementById("template_mediatheque").content;
                let newItem = document.importNode(content, true);

                if (elem.type == "video") {
                    let video = document.createElement("video");
                    video.src = elem.lien;
                    video.controls = true;
                    video.classList.add("vid_mediatheque")
                    newItem.querySelector(".media_container").appendChild(video);
                }
                if (elem.type == "musique") {
                    let audio = document.createElement("audio");
                    audio.src = elem.lien;
                    audio.controls = true;
                    newItem.querySelector(".media_container").appendChild(audio);
                }

                newItem.querySelector(".mediatheque_container h2").textContent = elem.titre;
                newItem.querySelector(".mediatheque_container p").textContent = elem.description;
                parentItems.appendChild(newItem);
            });
        },

        initButtons: function() {
            v.buttons = document.querySelectorAll('.buttons .btn');
            v.buttons.forEach(function(elem) {
                elem.addEventListener('click', function(e) {
                    v.removeClasses();
                    this.classList.add('active');
                    m.category = e.target.dataset.id;
                    c.setItemsByCategory();
                });
            });
        },

        removeClasses: function() {
            v.buttons.forEach(function(elem, index) {
                elem.classList.remove('active');
            })
        }
    }

    c.init();


    //})
</script>