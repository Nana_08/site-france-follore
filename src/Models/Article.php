<?php

class Article 
{

    private $id; 
    private $nom; 
    private $date; 
    private $media; 
    private $description;
    private $type;

    /**
     * Article constructor.
     * @param $id
     * @param $nom
     * @param $date
     * @param $media
     * @param $description
     * @param $type
     */
    public function __construct(int $id, $nom, $date, $media, $description, $type)
    {
        $this->id = $id;
        $this->nom = $nom;
        $date = new DateTime($date);
        $this->date = $date->format('d-m-Y');
        $this->media = $media;
        $this->description = $description;
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $date = new DateTime($date);
        $this->date = $date->format('d-m-Y');
    }

    /**
     * @return mixed
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param mixed $media
     */
    public function setMedia($media): void
    {
        $this->media = $media;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $description
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    public function toArray(){
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'date' => $this->date,
            'description' => $this->description,
            'media' => $this->media,
            'type' => $this->type
        ];
    }
}