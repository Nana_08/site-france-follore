<section>
    <?php require("templates/template_navbar.php"); ?>
    <h1 id="title_page">Les évènements</h1>
</section>


<select name="departementEvents" id="departementEvents">
    <option value="selectionner">Sélectionner votre département</option>
    <?php
    foreach ($dpts as $dpt) {
        echo '<option value="' . $dpt . '">' . $dpt . '</option>';
    }
    ?>
</select>
<section id="events">
    <?php
    foreach ($events as $event) {
        require('templates/template_events.php');
    }
    ?>
</section>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        // script qui permet de trier les évenements par département
        let select = document.querySelector("#departementEvents");
        let articles = JSON.parse('<?php echo $eventsJson; ?>');

        select.addEventListener('change', function(e) {
            clear();

            let articlesFiltered = articles;

            if (e.target.value !== "") {
                articlesFiltered = articles.filter(function(elem) {
                    return parseInt(elem.departement) === parseInt(e.target.value);
                });
            }

            articlesFiltered.forEach(function(elem) {
                renderArticle(elem)
            });

            if (articlesFiltered.length === 0) {
                let article = document.createElement('article');
                let section = document.querySelector("#events");
                let titre = document.createElement("h3");

                article.className = "col-5";
                article.id = "template_events";
                titre.textContent = ("Il n'y a aucun évènement dans ce département");

                article.appendChild(titre);
                section.appendChild(article);
            }

        });

        function clear() {
            let articles = document.querySelector('#events')
            articles.innerHTML = "";
        }

        function renderArticle(elem) {
            let article = document.createElement('article');
            let section = document.querySelector("#events");
            let titre = document.createElement("h3");
            let date = document.createElement("p")
            let div1 = document.createElement("div")
            let lieu = document.createElement("span")
            let div2 = document.createElement("div")
            let association = document.createElement("span")
            let description = document.createElement("p")
            let bouton = document.createElement("button")

            article.className = "col-5";
            article.id = "template_events";

            titre.textContent = elem.nom;
            article.appendChild(titre);

            date.id = "date";
            date.textContent = elem.date;
            article.appendChild(date);

            lieu.textContent = elem.localisation;
            div1.appendChild(lieu);

            association.textContent = elem.organisateur;
            div2.appendChild(association);

            article.appendChild(div1);
            article.appendChild(div2);

            description.textContent = elem.description;

            article.appendChild(description);
            bouton.id = "adAgenda";
            bouton.textContent = "Ajouter à l'agenda";

            article.appendChild(bouton);
            section.appendChild(article);
            console.log(article);
        }

        // script qui permet d'ajouter des évents au Google Calendar de l'utilisateur
        let events = document.querySelectorAll("#adAgenda");

        events.forEach(e => {
            e.addEventListener("click", agenda);
        })

        function agenda(e) {
            let titre = e.path[1].childNodes[1].textContent;
            let date = e.path[1].childNodes[3].textContent;

            let date_conv = date.split('-');
            let annee = date_conv[2];
            let mois = date_conv[1];
            let jour = date_conv[0];
            let jour_plus = parseInt(jour) + 1;

            let lieux = e.path[1].childNodes[5].textContent;
            let description = e.path[1].childNodes[9].textContent;

            let phase = "";
            let separator = description.split(' ')
            separator.forEach(e => {
                phase = phase + e + "+"
            })

            let lien = "https://calendar.google.com/calendar/r/eventedit?" +
                "text=" + titre + "&" +
                "dates=" + annee + mois + jour + "/" + annee + mois + jour_plus +
                "&details=" + description + "&" +
                "location=" + lieux;

            window.open(lien);
        }
    })
</script>