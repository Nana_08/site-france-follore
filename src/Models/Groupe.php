<?php

class Groupe
{

    private $id;
    private $nom;
    private $description;
    private $dpt;
    private $media;
    private $reseaux;
    private $site;

    public function __construct(int $id, $nom, $description, $dpt, $media, $reseaux, $site)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->description = $description;
        $this->dpt = $dpt;
        $this->media = $media;
        $this->reseaux = $reseaux;
        $this->site = $site;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }


    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDpt()
    {
        return $this->dpt;
    }

    /**
     * @param mixed $dpt
     */
    public function setDpt($dpt): void
    {
        $this->dpt = $dpt;
    }

    /**
     * @return mixed
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param mixed $media
     */
    public function setMedia($media): void
    {
        $this->media = $media;
    }

    /**
     * @return mixed
     */
    public function getReseaux()
    {
        return $this->reseaux;
    }

    /**
     * @param mixed $reseaux
     */
    public function setReseaux($reseaux): void
    {
        $this->reseaux = $reseaux;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     */
    public function setSite($site): void
    {
        $this->site = $site;
    }


    public function toArray()
    {
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'description' => $this->description,
            'dpt' => $this->dpt,
            'media' => $this->media,
            'reseaux' => $this->reseaux,
            'site' => $this->site
        ];
    }
}
