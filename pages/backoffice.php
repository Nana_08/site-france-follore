<?php
function jsonSerialize($elem)
{
    return $elem->toArray();
}

$articlesBoutique = [];
foreach ($boutiques as $boutique) {
    array_push($articlesBoutique, jsonSerialize($boutique));
}

$articlesSerialized = [];
foreach ($articles as $article) {
    array_push($articlesSerialized, jsonSerialize($article));
}

$eventsSerialized = [];
foreach ($events as $event) {
    array_push($eventsSerialized, jsonSerialize($event));
}

$mediasSerialized = [];
foreach ($mediatheques as $medias) {
    array_push($mediasSerialized, jsonSerialize($medias));
}
?>

<!-- Le css ici ne sera lu que pour cette page -->
<style>
    .event,
    .boutique,
    .mediatheque {
        display: none;
    }

    td {
        text-align: center;
    }

    i.icon {
        cursor: pointer;
        margin: 0 10px;
    }

    .box {
        height: 500px;
    }

    section.box>h2 {
        text-align: center;
        padding: 20px;
    }

    table {
        border-collapse: collapse;
        margin: auto;
    }

    tr td,
    tr th {
        padding: 5px;
        border-bottom: 1px solid black;
    }

    tr:last-of-type td {
        border-bottom: none;
    }

    #create-data {
        position: relative;
        margin-left: 70%;
    }

    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 2%;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        border-radius: 10px;
    }

    .modal-content p {
        text-align: center;
    }

    .modal-title {
        padding-bottom: 20px;
        text-align: center;
    }

    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    form {
        height: inherit;
    }

    #pager {
        display: flex;
        margin: auto;
        justify-content: center;
        margin-top: 15px;
    }

    .num-pager {
        padding: 5px 10px;
        cursor: pointer;
        border: 1px solid black;
    }

    .num-pager:hover,
    .num-pager.active {
        background-color: var(--blue-color);
        color: white;
    }

    select {
        margin: 0 !important;
    }
</style>

<section id="page-backoffice">
    <?php require("templates/template_navbar.php"); ?>
    <h1 id="title_page">Gestion du site</h1>
    <section class="main">
        <div class="col-8 container">
            <button class="btn active" id="article">Gérer les articles</button>
            <button class="btn" id="event">Gérer les évenements</button>
            <button class="btn" id="boutique">Gérer la boutique</button>
            <button class="btn" id="mediatheque">Gérer la médiathèque</button>
        </div>

        <section id="data" class="box col-7">
            <h2 id="title_section"></h2>
            <table class="col-11">
                <thead>
                    <tr>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div id="pager" class="col-8"></div>
            <button class="btn" id="create-data">Créer une nouvelle entrée <i class="fa fa-plus-circle"></i></button>
            <?php
            if (isset($success)) {
                echo '<div class="success col-8">' . $success . '</div>';
            }
            ?>
        </section>
    </section>
    <div id="modal" class="modal">
        <div class="modal-content col-6">

        </div>
    </div>
</section>

<script type="text/javascript">
    var articles = <?php echo json_encode($articlesSerialized) ?>;
    var events = <?php echo json_encode($eventsSerialized) ?>;
    var boutiques = <?php echo json_encode($articlesBoutique) ?>;
    var mediatheques = <?php echo json_encode($mediasSerialized) ?>;
    let title = document.querySelector('#title_section');
    <?php
    if (isset($success)) {
    ?>
        let divSuccess = document.querySelector('div.success');
        setTimeout(function() {
            divSuccess.style.display = "none";
        }, 5000)
    <?php
    }
    ?>

    var pager = document.getElementById('pager');
    var page = 1;
    title.innerText = "Gérer les articles";
    document.addEventListener('DOMContentLoaded', function() {
        let buttons = document.querySelectorAll(".container>button");
        buttons.forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                removeClasses();
                this.classList.add('active');
                let title = document.querySelector('#title_section');
                title.innerText = this.innerText;
            });
        });

        function removeClasses() {
            buttons.forEach(function(elem, index) {
                elem.classList.remove('active');
            })
        }

        let btnCreate = document.getElementById('create-data');
        btnCreate.addEventListener('click', function() {
            switch (document.querySelector('#title_section').innerText) {
                case "Gérer les articles":
                    initModalCreateArticle();
                    break;
                case "Gérer les évenements":
                    initModalCreateEvent();
                    break;
                case "Gérer la boutique":
                    initModalCreateBoutique();
                    break;
            }
        });
    })
</script>
<script type="text/javascript" src="../public/script/utils.js"></script>
<script type="text/javascript" src="../public/script/manageArticle.js"></script>
<script type="text/javascript" src="../public/script/manageEvents.js"></script>
<script type="text/javascript" src="../public/script/manageBoutique.js"></script>
<script type="text/javascript" src="../public/script/manageMediatheque.js"></script>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        let buttonArticle = document.getElementById("article");
        buttonArticle.click();
    });
</script>