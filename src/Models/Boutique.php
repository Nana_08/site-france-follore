<?php

class Boutique
{
    private $id;
    private $type;
    private $titre;
    private $description;
    private $image;
    private $prix;

    /**
     * Boutique constructor.
     * @param $id
     * @param $type
     * @param $titre
     * @param $description
     * @param $image
     * @param $prix
     */

    public function __construct($id, $type, $titre, $description, $image, $prix)
    {
        $this->id = $id;
        $this->type = $type;
        $this->titre = $titre;
        $this->description = $description;
        $this->image = $image;
        $this->prix = $prix;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre): void
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix): void
    {
        $this->prix = $prix;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'titre' => $this->getTitre(),
            'description' => $this->getDescription(),
            'prix' => $this->getPrix(),
            'image' => $this->getImage(),
        ];
    }
}
