DROP TABLE IF EXISTS `db_user`;

CREATE TABLE `db_user` (
                               `id` int(11) NOT NULL AUTO_INCREMENT,
                               `login` varchar(256) NOT NULL,
                               `password` varchar(256) NOT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;