<section id="page-login">
    <?php require("templates/template_navbar.php"); ?>
    <h1 id="title_page">Connexion</h1>
    <section id="login" class="box col-7">
        <form method="POST" action="../index.php?page=login">
            <div class="form-group">
                <label for="login-input" class="col-4">Identifiant:</label>
                <input type="text" class="col-4" placeholder="Identifiant..." name="login" id="login-input">
            </div>
            <div class="form-group">
                <label for="password-input" class="col-4">Mot de passe:</label>
                <input type="password" class="col-4" placeholder="Mot de passe..." name="password" id="password-input">
            </div>
            <?php
            if (isset($error)) {
                echo "<div class='error col-10'>
                        <p >$error</p>
                       </div>";
            }
            ?>
            <button type="submit" class="btn">Se connecter</button>
        </form>
    </section>
</section>


<?php


?>