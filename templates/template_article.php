
<!-- template PHP Dynamique pour tous les articles -->
<article class="col-5">
        <h3><?php echo $article->getNom()?></h3>
        <p id="date"><?php echo $article->getDate() ?></p>
        <?php if($article->getMedia() !== "") { ?>
                <img src="<?php echo $article->getMedia()?>" alt="photo <?php echo $article->getNom(); ?>">
        <?php } ?>
        <p><?php echo $article->getDescription()?></p>
</article>