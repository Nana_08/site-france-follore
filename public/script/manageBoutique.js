let buttonBoutique = document.getElementById("boutique");
let columnBoutiques = ["Nom de l'article", "Type de l'article", "Description de l'article", "Prix de l'article", "Actions"]

buttonBoutique.addEventListener('click', function() {
    pager.innerHTML = '';
    page = 1;

    initBoutique();
    initPager(boutiques, initBoutique);

    closeModalOutsideClick(modal);
});

function initBoutique() {
    let thead = document.querySelector("thead>tr");
    thead.innerHTML = "";
    let tbody = document.querySelector("tbody");
    tbody.innerHTML = "";
    let modal = document.getElementById('modal');
    /** On ajoute les colonnes dans le DOM **/
    columnBoutiques.forEach(function(elem) {
        let th = document.createElement("th");
        th.innerText = elem;
        if (elem === "Type de l'article" || elem === "Prix de l'article") {
            th.classList.add('col-2')
        } else {
            th.classList.add('col-3');
        }
        thead.appendChild(th);
    });

    /** Pour chaque boutiques, on ajoute les datas associés **/
    groupPerPage(boutiques)[page].forEach(function(boutique) {
        let tr = document.createElement("tr");
        let boutiqueCopy = {...boutique };
        delete boutiqueCopy.id;
        delete boutiqueCopy.image;


        let keys = ['titre', 'type', 'description', 'prix']

        keys.forEach(function(elem) {
            let td = document.createElement("td");
            boutique[elem].length > 20 ? td.innerText = boutique[elem].substr(0, 20) + '...' : td.innerText = boutique[elem];
            tr.appendChild(td);
        });

        addActions(tr, boutique);
        tbody.appendChild(tr);
    })

    /** Pour chaque icon, on rajoute un event listener "onclick", permet de faire une action **/
    let icons = document.querySelectorAll("td>i.icon");
    icons.forEach(function(elem) {
        elem.addEventListener("click", function(e) {
            let idBoutique = parseInt(e.currentTarget.parentNode.dataset.id);
            /** On récupère l'article que l'on modifie **/
            let currentBoutique = boutiques.find(function(elem) {
                if (parseInt(elem.id) === idBoutique) {
                    return elem;
                }
            });

            if (elem.classList.contains("fa-edit")) {
                let modalContentEdit = document.querySelector('#modal>.modal-content');
                /** On ajoute le contenu de la modal **/
                modalContentEdit.innerHTML = `
                        <span class="close">&times;</span>
                        <h3 class="modal-title">Edition de l'article</h3>
                        <form method="POST" action="index.php?page=backoffice&mode=edit" >
                            <div class="form-group">
                                <label class="col-4" for="nom">Nom de l'article :</label>
                                <input type="text" class="col-6" placeholder="Nom de l'article..." id="titre" name="titre" value="` + currentBoutique.titre + `"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="type-boutique">Type de l'article :</label>
                                <select class="col-6" id="type-boutique" name="type-boutique">
                                    <option value="">Séléctionner un type...</option>
                                    <option value="cd">CD</option>
                                    <option value="livres">Livres</option>
                                    <option value="dvd">DVD</option>
                                    <option value="goodies">Goodies</option>
                                    <option value="magazines">Magazines</option>
                                </select>
                            </div> 
                            <div class="form-group">
                                <label class="col-4" for="date">Description de l'article :</label>
                                <textarea class="col-6" placeholder="Description de l'article..." id="description" name="description">` + currentBoutique.description + `</textarea>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="prix">Prix de l'article :</label>
                                <input type="number" class="col-6" placeholder="Prix de l'article..." id="prix" name="prix" value="` + currentBoutique.prix + `"/>
                            </div>
                            <input type="hidden" value="` + currentBoutique.id + `" name="id"/>
                            <input type="hidden" value="boutique" name="type"/>
                            <button type="submit" class="btn">Editer</button>
                        </form>`;

                let select = document.getElementById('type-boutique');
                select.value = currentBoutique.type;
            } else if (elem.classList.contains("fa-trash-alt")) {
                let modalContentDelete = document.querySelector('#modal>.modal-content');
                modalContentDelete.innerHTML = `
                    <span class="close">&times;</span>
                    <h3 class="modal-title">Suppression de l'article</h3>
                    <p>Êtes vous sur de vouloir supprimer l'article " ` + currentBoutique.titre + ` " de la boutique?</p>
                    <form method="POST" action="index.php?page=backoffice&mode=delete">
                        <input type="hidden" value="` + currentBoutique.id + `" name="id"/>
                        <input type="hidden" value="boutique" name="type"/>
                        <button type="submit" class="btn">Supprimer</button>
                    </form>
                `;
            }
            modal.style.display = "block";
            /** Permet de fermer la modal au clique sur la croix **/
            var span = document.querySelector("span.close");

            span.onclick = function() {
                modal.style.display = "none";
            }
        })
    });
}



function initModalCreateBoutique() {
    let modal = document.getElementById('modal');
    let modalContentCreate = document.querySelector('#modal>.modal-content');

    modalContentCreate.innerHTML = `
        <span class="close">&times;</span>
                        <h3 class="modal-title">Création d'un article dans la boutique</h3>
                        <form method="POST" action="index.php?page=backoffice&mode=create" >
                            <div class="form-group">
                                <label class="col-4" for="titre">Nom de l'article :</label>
                                <input type="text" class="col-6" placeholder="Nom de l'article..." id="titre" name="titre"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="type-boutique">Type de l'article :</label>
                                <select class="col-6" id="type-boutique" name="type-boutique">
                                    <option value="">Séléctionner un type...</option>
                                    <option value="cd">CD</option>
                                    <option value="livres">Livres</option>
                                    <option value="dvd">DVD</option>
                                    <option value="goodies">Goodies</option>
                                    <option value="magazines">Magazines</option>
                                </select>
                            </div> 
                            <div class="form-group">
                                <label class="col-4" for="description">Description de l'article :</label>
                                <textarea class="col-6" placeholder="Description de l'article..." id="description" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="prix">Prix de l'article :</label>
                                <input type="number" class="col-6" placeholder="Prix de l'article..." id="prix" name="prix"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="media">Image de l'article :</label>
                                <input type="file" name="image" id="image" class="col-6"/>
                            </div>
                            <input type="hidden" name="id"/>
                            <input type="hidden" value="boutique" name="type"/>
                            <button type="submit" class="btn">Editer</button>
                        </form>
    `;
    modal.style.display = "block";

    var span = document.querySelector("span.close");
    span.onclick = function() {
        modal.style.display = "none";
    }

    closeModalOutsideClick(modal)
}