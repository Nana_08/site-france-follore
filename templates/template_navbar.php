<a class="menu_btn">
    <img src="public/img/svg/menu.svg" alt="bouton menu">
</a>
<div>

    <a id="logo" href="?page=accueil">
        <img src="public/img/logo.PNG" alt="logo France Folklore">
    </a>

    <nav>
        <a class="fermer_menu">
            <img src="public/img/svg/croix.svg" alt="fermer le menu">
        </a>
        <ul>
            <li>
                <a href="?page=le_mag">Le mag</a>
            </li>
            <li>
                <a href="?page=les_evenements">Les évènements</a>
            </li>
            <li>
                <a href="?page=mediatheque">La médiathèque</a>
            </li>
            <li>
                <a href="?page=la_confederation">La confédération</a>
            </li>
            <li>
                <a href="?page=boutique">La boutique</a>
            </li>
            <li>
                <a href="?page=services">Les services</a>
            </li>
            <li>
                <a href="?page=faq">Foire aux questions</a>
            </li>
            <li>
                <a target="_blank" href="https://www.france-folklore.fr/espace_membre/index.php?action=connexion">Intranet</a>
            </li>

        </ul>
</div>

</nav>

<script>
    let menu = document.querySelector(".menu_btn");
    let navbar = document.querySelector("nav");
    let fermer = document.querySelector(".fermer_menu");
    menu.addEventListener("click", function () {
        navbar.style.left = 0;
    });
    fermer.addEventListener("click", function () {
        navbar.style.left = "-100vw";
    });
</script>