<section>
    <?php require("templates/template_navbar.php"); ?>
    <h1 id="title_page">Le Mag</h1>
    <section id="articles">
        <?php

        /** Pour chaque articles, on appel le template HTML d'article */
        foreach($mags as $mag)
        {
            require('templates/template_mags.php');
        }
        ?>
    </section>
</section>

