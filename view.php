<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>France Folklore</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/utils.css">
</head>
<body>

    <?php require("pages/".$page.".php"); ?>
    <!-- Footer -->
    <?php require("templates/template_footer.html"); ?>
    <!-- Footer -->
</body>
</html>
<script src="https://kit.fontawesome.com/b935fe740d.js" crossorigin="anonymous"></script>
