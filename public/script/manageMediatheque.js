let buttonMediatheque = document.getElementById("mediatheque");
let columnMediatheques = ["Titre du média", "Type du média", "Description du média", "Actions"]

buttonMediatheque.addEventListener('click', function() {
    pager.innerHTML = '';
    page = 1;
    initMediatheque();
    initPager(events, initMediatheque);
    /** Si on clique a côté de la modal on ferme la modal **/
    closeModalOutsideClick(modal)
});

function initMediatheque() {
    pager.innerHTML = '';
    let thead = document.querySelector("thead>tr");
    thead.innerHTML = "";
    let tbody = document.querySelector("tbody");
    tbody.innerHTML = "";
    let modal = document.getElementById('modal');

    /** On ajoute les colonnes dans le DOM **/
    columnMediatheques.forEach(function(elem) {
        let th = document.createElement("th");
        th.innerText = elem;
        if (elem === 'Actions') {
            th.style.minWidth = '75px';
        }
        th.classList.add('col-4');

        thead.appendChild(th);
    });

    /** Pour chaque articles, on ajoute les datas associés **/
    groupPerPage(mediatheques)[page].forEach(function(media) {
        let tr = document.createElement("tr");
        let mediaCopy = {...media };
        delete mediaCopy.id;
        delete mediaCopy.lienMedia;

        let keys = ['titre', 'type', 'description'];

        keys.forEach(function(elem) {
            let td = document.createElement("td");
            td.innerText = media[elem].length > 20 ? media[elem].substr(0, 20) + '...' : media[elem];
            tr.appendChild(td);
        });

        addActions(tr, media);
        tbody.appendChild(tr);
    })

    /** Pour chaque icon, on rajoute un event listener "onclick", permet de faire une action **/
    let icons = document.querySelectorAll("td>i.icon");
    icons.forEach(function(elem) {
        elem.addEventListener("click", function(e) {
            let idEvent = parseInt(e.currentTarget.parentNode.dataset.id);
            /** On récupère l'article que l'on modifie **/
            let currentEvent = events.find(function(elem) {
                if (parseInt(elem.id) === idEvent) {
                    return elem;
                }
            });
            if (elem.classList.contains("fa-edit")) {
                currentEvent.date = formatDate(currentEvent.date);
                let modalContentEdit = document.querySelector('#modal>.modal-content');
                /** On ajoute le contenu de la modal **/
                modalContentEdit.innerHTML = `
                        <span class="close">&times;</span>
                        <h3 class="modal-title">Edition du média</h3>
                        <form method="POST" action="index.php?page=backoffice&mode=edit">
                            <div class="form-group">
                                <label class="col-4" for="nom">Nom de l'évenement :</label>
                                <input type="text" class="col-6" placeholder="Nom de l'évenement..." id="nom" name="nom" value="` + currentEvent.nom + `"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="date">Date de l'évenement :</label>
                                <input type="date" class="col-6" placeholder="Date de l'évenement..." id="date" name="date" value="` + currentEvent.date + `"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="nom">Organisateur de l'évenement :</label>
                                <input type="text" class="col-6" placeholder="Organisateur de l'évenement..." id="organisateur" name="organisateur" value="` + currentEvent.organisateur + `"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="nom">Département de l'évenement :</label>
                                <input type="text" class="col-6" placeholder="Département de l'évenement..." id="departement" name="departement" value="` + currentEvent.departement + `"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="nom">Localisation de l'évenement :</label>
                                <input type="text" class="col-6" placeholder="Localisation de l'évenement..." id="localisation" name="localisation" value="` + currentEvent.localisation + `"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="date">Description de l'évenement :</label>
                                <textarea class="col-6" placeholder="Description de l'évenement..." id="description" name="description">` + currentEvent.description + `</textarea>
                            </div>
                            <input type="hidden" value="` + currentEvent.id + `" name="id"/>
                            <input type="hidden" value="media" name="type"/>
                            <button type="submit" class="btn">Editer</button>
                        </form>`;
            } else if (elem.classList.contains("fa-trash-alt")) {
                let modalContentDelete = document.querySelector('#modal>.modal-content');
                modalContentDelete.innerHTML = `
                    <span class="close">&times;</span>
                    <h3 class="modal-title">Suppression de l'évenement</h3>
                    <p>Êtes vous sur de vouloir supprimer l'évenement " ` + currentEvent.nom + ` " ?</p>
                    <form method="POST" action="index.php?page=backoffice&mode=delete">
                        <input type="hidden" value="` + currentEvent.id + `" name="id"/>
                        <input type="hidden" value="article" name="type"/>
                        <button type="submit" class="btn">Supprimer</button>
                    </form>
                `;
            }
            modal.style.display = "block";
            /** Permet de fermer la modal au clique sur la croix **/
            var span = document.querySelector("span.close");

            span.onclick = function() {
                modal.style.display = "none";
            }
        })
    });
}

function initModalCreateEvent() {
    let modal = document.getElementById('modal');
    let modalContentCreate = document.querySelector('#modal>.modal-content');

    modalContentCreate.innerHTML = `
        <span class="close">&times;</span>
                        <h3 class="modal-title">Création d'un évenement</h3>
                        <form method="POST" action="index.php?page=backoffice&mode=create">
                            <div class="form-group">
                                <label class="col-4" for="nom">Nom de l'évenement :</label>
                                <input type="text" class="col-6" placeholder="Nom de l'évenement..." id="nom" name="nom"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="date">Date de l'évenement :</label>
                                <input type="date" class="col-6" placeholder="Date de l'évenement..." id="date" name="date"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="nom">Organisateur de l'évenement :</label>
                                <input type="text" class="col-6" placeholder="Organisateur de l'évenement..." id="organisateur" name="organisateur"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="nom">Département de l'évenement :</label>
                                <input type="text" class="col-6" placeholder="Département de l'évenement..." id="departement" name="departement"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="nom">Localisation de l'évenement :</label>
                                <input type="text" class="col-6" placeholder="Localisation de l'évenement..." id="localisation" name="localisation"/>
                            </div>
                            <div class="form-group">
                                <label class="col-4" for="date">Description de l'évenement :</label>
                                <textarea class="col-6" placeholder="Description de l'évenement..." id="description" name="description"></textarea>
                            </div>
                            <input type="hidden" value="event" name="type"/>
                            <button type="submit" class="btn">Editer</button>
                        </form>
    `;
    modal.style.display = "block";

    var span = document.querySelector("span.close");
    span.onclick = function() {
        modal.style.display = "none";
    }

    /** Si on clique a côté de la modal on ferme la modal **/
    closeModalOutsideClick(modal)
}