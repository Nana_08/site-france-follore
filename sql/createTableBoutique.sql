DROP TABLE IF EXISTS `boutique`;
CREATE TABLE `boutique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(256) NOT NULL,
  `titre` varchar(256) NOT NULL,
  `description` varchar(1500) NOT NULL,
  `image` varchar(256) NOT NULL,
  `prix` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
