
DROP TABLE IF EXISTS `mediatheque`;

CREATE TABLE `mediatheque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(256) NOT NULL,
  `titre` varchar(256) NOT NULL,
  `description` varchar(800) NOT NULL,
  `lien_media` varchar(300) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
