<?php

require("Models/Article.php");
require("Models/Event.php");
require("Models/Groupe.php");
require("Models/Boutique.php");
require("Models/Mediatheque.php");

class DBInterface
{
    private $PDO;

    /**
     * DBInterface constructor.
     */
    public function __construct()
    {
        try {
            $this->PDO = new PDO("mysql:dbname=france_folklore;host=localhost:3306; charset=utf8", "andrea", "andrea");
            $this->PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $exception) {
            /** @var TYPE_NAME $exception */
            throw $exception->getMessage();
        }
    }

    public function getPDO()
    {
        return $this->PDO;
    }

    public function date_convert($date)
    {
        $date = explode('-', $date);
        return ($date[2] . '/' . $date[1] . '/' . $date[0]);
    }

    /**
     * Récupération de tous les articles depuis la base de données
     */
    public function getAllArticles(): array
    {
        /** Permet de sécuriser un appel à la base de données en "préparant" sa requête */
        $stmt = $this->PDO->prepare('SELECT * FROM Articles WHERE type = "article"');
        $stmt->execute();
        /** Récupération des datas */
        $articles = $stmt->fetchAll();

        $formattedArticles = [];

        /** On reformat toutes les datas grâce à la fonction formatArticle */
        foreach ($articles as $article) {
            $formattedArticles[] = $this->formatArticle($article);
        }

        return $formattedArticles;
    }

    /** Permet de traduire une ligne de base de données en un Objet Article */
    public function formatArticle($item): Article
    {
        $article = new Article($item['id'], $item['nom'], $item['date'], $item['media'], $item['description'], $item['type']);
        return $article;
    }

    /**
     * Récupération de tous les articles depuis la base de données
     */
    public function getAllMags()
    {
        /** Permet de sécuriser un appel à la base de données en "préparant" sa requête */
        $stmt = $this->PDO->prepare('SELECT * FROM articles WHERE type = "mag" ');
        $stmt->execute();
        /** Récupération des datas */
        $mags = $stmt->fetchAll();

        $formattedMags = [];

        /** On reformat toutes les datas grâce à la fonction formatArticle */
        foreach ($mags as $mag) {
            $formattedMags[] = $this->formatArticle($mag);
        }
        return $formattedMags;
    }

    public function getAllEvents()
    {
        $stmt = $this->PDO->prepare('SELECT * FROM events');
        $stmt->execute();
        $events = $stmt->fetchAll();

        $formattedEvents = [];

        foreach ($events as $event) {
            $formattedEvents[] = $this->formatEvent($event);
        }
        return $formattedEvents;
    }

    public function formatEvent($item): Event
    {
        $event = new Event($item['id'], $item['nom'], $item['date'], $item['localisation'], $item['departement'], $item['description'], $item['organisateur']);
        return $event;
    }

    public function getDpts(array $events): array
    {
        $dpt = [];

        foreach ($events as $key => $event) {
            if (!isset($dpt[$event->getDepartement()])) {
                $dpt[$event->getDepartement()] = $key;
            }
        }
        $dpt = array_flip($dpt);
        return $dpt;
    }

    public function getAllBoutique()
    {
        $stmt = $this->PDO->prepare('SELECT * FROM boutique');
        $stmt->execute();
        $boutique = $stmt->fetchAll();

        $formattedBoutique = [];

        foreach ($boutique as $article) {
            $formattedBoutique[] = $this->formatBoutique($article);
        }
        return $formattedBoutique;
    }

    public function formatBoutique($item): Boutique
    {
        $event = new Boutique($item['id'], $item['type'], $item['titre'], $item['description'], $item['image'], $item['prix']);

        return $event;
    }

    public function getAllMediatheque()
    {
        $stmt = $this->PDO->prepare('SELECT * FROM mediatheque');
        $stmt->execute();
        $boutique = $stmt->fetchAll();

        $formattedMediatheque = [];

        foreach ($boutique as $article) {
            $formattedMediatheque[] = $this->formatMediatheque($article);
        }
        return $formattedMediatheque;
    }

    public function formatMediatheque($item): Mediatheque
    {
        $medias = new Mediatheque($item['id'], $item['type'], $item['titre'], $item['description'], $item['lien_media']);

        return $medias;
    }

    public function getAllServices()
    {
        $stmt = $this->PDO->prepare('SELECT * FROM services');
        $stmt->execute();
        $services = $stmt->fetchAll();
        $description = [];
        foreach ($services as $service) {
            $stmt = $this->PDO->prepare('SELECT DISTINCT sub.* FROM services s INNER JOIN sub_services sub WHERE sub.service_id = :id');
            $stmt->bindParam(":id", $service['id']);
            $stmt->execute();
            $description[$service['id']] = $services;
            $description["sub" . $service['id']] = $stmt->fetchAll();
        }

        return $description;
    }

    public function getAllGroupes(): array
    {
        $stmt = $this->PDO->prepare('SELECT * FROM groupes');
        $stmt->execute();
        $groupes = $stmt->fetchAll();
        $formattedGroupes = [];
        foreach ($groupes as $groupe) {
            $formattedGroupes[] = $this->formatGroupe($groupe);
        }
        return $formattedGroupes;
    }

    public function formatGroupe($item): Groupe
    {
        $groupe = new Groupe($item['id'], $item['nom'], $item['description'], $item['dpt'], $item['media'], $item['reseaux'], $item['site']);
        return $groupe;
    }

    public function authentication($login, $password)
    {
        $stmt = $this->PDO->prepare('SELECT login, password FROM db_user WHERE login = :login');
        $stmt->bindParam(':login', $login);
        $stmt->execute();
        $user = $stmt->fetchAll();
        if (!empty($user)) {
            if (password_verify($password, $user[0]['password'])) {
                return "Error: Mot de passe ou identifiant erroné";
            } else {
                return ["user" => $user];
            }
        } else {
            return "Error: Mot de passe ou identifiant erroné";
        }
    }

    public function createArticle($nom, $date, $description, $media)
    {
        move_uploaded_file($media['tmp_name'], 'resources/pictures/articles/' . $media['name']);
        $mediaPath = 'resources/pictures/articles/' . $media['name'];
        $stmt = $this->PDO->prepare('INSERT INTO articles (nom, date, description, media, type) VALUES (:nom, :date, :description, :media, "article")');
        $stmt->bindParam(':nom', $nom);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':media', $mediaPath);
        $stmt->execute();

        return "L'article a été créé avec succès.";
    }

    public function editArticle($id, $nom, $date, $description)
    {
        $stmt = $this->PDO->prepare('UPDATE articles set nom = :nom, date =:date, description = :description WHERE id = :id');
        $stmt->bindParam(':nom', $nom);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return "L'article a été modifié avec succès.";
    }

    public function deleteArticle($id)
    {
        $stmt = $this->PDO->prepare('DELETE FROM articles WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return "L'article a été supprimé avec succès.";
    }

    public function createEvent($nom, $date, $organisateur, $localisation, $departement, $description)
    {
        $stmt = $this->PDO->prepare('INSERT INTO events (nom, date, localisation, departement, organisateur, description) VALUES (:nom, :date, :localisation, :departement, :organisateur, :description)');
        $stmt->bindParam(':nom', $nom);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':localisation', $localisation);
        $stmt->bindParam(':departement', $departement);
        $stmt->bindParam(':organisateur', $organisateur);
        $stmt->bindParam(':description', $description);
        $stmt->execute();

        return "L'évenement a été créé avec succès.";
    }

    public function editEvent($id, $nom, $date, $organisateur, $localisation, $departement, $description)
    {
        $stmt = $this->PDO->prepare('UPDATE events set nom = :nom, date =:date, description = :description, organisateur = :organisateur, localisation = :localisation, departement = :departement WHERE id = :id');
        $stmt->bindParam(':nom', $nom);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':departement', $departement);
        $stmt->bindParam(':organisateur', $organisateur);
        $stmt->bindParam(':localisation', $localisation);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return "L'évenement a été modifié avec succès.";
    }

    public function deleteEvent($id)
    {
        $stmt = $this->PDO->prepare('DELETE FROM events WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return "L'évenement a été supprimé avec succès.";
    }

    public function createBoutique($titre, $type, $prix, $description, $image)
    {
        $imagePath = 'resources/pictures/boutique/' . $image['name'];
        move_uploaded_file($image['tmp_name'], $imagePath);
        $stmt = $this->PDO->prepare('INSERT INTO boutique (titre, type, description, image, prix) VALUES (:titre, :type, :description, :image, :prix)');
        $stmt->bindParam(':titre', $titre);
        $stmt->bindParam(':type', $type);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':image', $imagePath);
        $stmt->bindParam(':prix', $prix);
        $stmt->execute();

        return "L'article a été créé dans la boutique avec succès.";
    }

    public function editBoutique($id, $titre, $type, $prix, $description)
    {
        $stmt = $this->PDO->prepare('UPDATE boutique set titre = :titre, type =:type, description = :description, prix = :prix WHERE id = :id');
        $stmt->bindParam(':titre', $titre);
        $stmt->bindParam(':type', $type);
        $stmt->bindParam(':prix', $prix);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return "L'article a été modifié dans la boutique avec succès.";
    }

    public function deleteBoutique($id)
    {
        $stmt = $this->PDO->prepare('DELETE FROM boutique WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return "L'article a été supprimé de la boutique avec succès.";
    }
}
