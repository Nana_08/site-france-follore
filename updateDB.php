<?php

$PDO = new PDO("mysql:host=localhost:3306; charset=utf8", "andrea", "andrea");
$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//creation DB
$create = file_get_contents('sql/createDB.sql');

$stmt = $PDO->prepare($create);
$stmt->execute();
$PDO = new PDO("mysql:dbname=france_folklore;host=localhost:3306; charset=utf8", "andrea", "andrea");
$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//creation Table Articles
$createArticles = file_get_contents('sql/createTableArticles.sql');

$stmt = $PDO->prepare($createArticles);
$stmt->execute();

echo "===== \033[32m TABLE \033[0m ARTICLES \033[32m CREATED SUCCESSFULLY \033[0m ===== \n";


//creation Table Events
$createEvents = file_get_contents('sql/createTableEvents.sql');

$stmt = $PDO->prepare($createEvents);
$stmt->execute();

echo "===== \033[32m TABLE \033[0m EVENTS \033[32m CREATED SUCCESSFULLY \033[0m ===== \n";

//creation Table Boutique
$createBoutique = file_get_contents('sql/createTableBoutique.sql');

$stmt = $PDO->prepare($createBoutique);
$stmt->execute();
echo "===== \033[32m TABLE \033[0m BOUTIQUE \033[32m CREATED SUCCESSFULLY \033[0m ===== \n";

//creation Table Mediatheque
$createMediatheque = file_get_contents('sql/createTableMediatheque.sql');

$stmt = $PDO->prepare($createMediatheque);
$stmt->execute();
echo "===== \033[32m TABLE \033[0m MEDIATHEQUE \033[32m CREATED SUCCESSFULLY \033[0m ===== \n";

//creation Table DB_USER
$createUser = file_get_contents('sql/createTableUser.sql');

$stmt = $PDO->prepare($createUser);
$stmt->execute();
echo "===== \033[32m TABLE \033[0m DB_USER \033[32m CREATED SUCCESSFULLY \033[0m ===== \n";

//creation Table services
$services = file_get_contents('sql/services.sql');

$stmt = $PDO->prepare($services);
$stmt->execute();
echo "===== \033[32m TABLE \033[0m SERVICES \033[32m CREATED SUCCESSFULLY \033[0m ===== \n";

//creation Table services
$sub_services = file_get_contents('sql/sub_services.sql');

$stmt = $PDO->prepare($sub_services);
$stmt->execute();
echo "===== \033[32m TABLE \033[0m SUB_SERVICES \033[32m CREATED SUCCESSFULLY \033[0m ===== \n";

//creation Table groupes
$createGroupes = file_get_contents('sql/createTableGroupes.sql');

$stmt = $PDO->prepare($createGroupes);
$stmt->execute();
echo "===== \033[32m TABLE \033[0m GROUPES \033[32m CREATED SUCCESSFULLY \033[0m ===== \n";

//chargement All Data
$insertData = file_get_contents('sql/insertAllData.sql');

$stmt = $PDO->prepare($insertData);
$stmt->execute();

echo "===== \033[32m ALL DATA LOADED SUCCESSFULLY \033[0m ===== \n";
