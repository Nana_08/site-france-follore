<?php
require("src/DBInterface.php");

$db = new DBInterface();
session_start();
// ?? équivalent ISSET
$page = $_REQUEST['page'] ?? 'accueil';

if (isset($_POST) && !empty($_POST)) {
    switch ($page) {
        case "login":
            $result = $db->authentication($_POST['login'], $_POST['password']);
            if (gettype($result) !== 'array') {
                $error = $result;
            } else {
                $_SESSION['user'] = $result;
                header('Location: index.php?page=backoffice');
            }
            break;
        case "backoffice":
            $type = $_POST['type'];
            switch ($type) {
                case "article":
                    if ($_REQUEST['mode'] === 'edit') {
                        $success = $db->editArticle($_POST['id'], $_POST['nom'], $_POST['date'], $_POST['description']);
                    } else if ($_REQUEST['mode'] === 'delete') {
                        $success = $db->deleteArticle($_POST['id']);
                    } else if ($_REQUEST['mode'] === 'create') {
                        $success = $db->createArticle($_POST['nom'], $_POST['date'],  $_POST['description'], $_POST['media']);
                    }
                    break;
                case "event":
                    if ($_REQUEST['mode'] === 'edit') {
                        $success = $db->editEvent($_POST['id'], $_POST['nom'], $_POST['date'], $_POST['organisateur'], $_POST['localisation'], $_POST['departement'], $_POST['description']);
                    } else if ($_REQUEST['mode'] === 'delete') {
                        $success = $db->deleteEvent($_POST['id']);
                    }
                    if ($_REQUEST['mode'] === 'create') {
                        $success = $db->createEvent($_POST['nom'], $_POST['date'], $_POST['organisateur'], $_POST['localisation'], $_POST['departement'], $_POST['description']);
                    }
                    break;
                case "boutique":
                    if ($_REQUEST['mode'] === 'edit') {
                        $success = $db->editBoutique($_POST['id'], $_POST['titre'], $_POST['type'], $_POST['prix'], $_POST['description']);
                    } else if ($_REQUEST['mode'] === 'delete') {
                        $success = $db->deleteBoutique($_POST['id']);
                    }
                    if ($_REQUEST['mode'] === 'create') {
                        $success = $db->createBoutique($_POST['titre'], $_POST['type-boutique'], $_POST['prix'], $_POST['description'], $_FILES['image']);
                    }
                    break;
            }
    }
}

switch ($page) {
    case 'accueil':
        $articles = $db->getAllArticles();
        break;
    case 'le_mag':
        $mags = $db->getAllMags();
        break;
    case 'les_evenements':
        $events = $db->getAllEvents();
        $dpts = $db->getDpts($events);
        $eventsJson = json_encode(array_map(function ($elem) {
            return $elem->toArray();
        }, $db->getAllEvents()));
        break;
    case 'boutique':
        $boutique = $db->getAllBoutique();
        break;
    case 'mediatheque':
        $mediatheque = $db->getAllMediatheque();
        break;
    case 'backoffice':
        if (!isset($_SESSION['user'])) {
            header('Location: index.php?page=login');
        }
        $boutiques = $db->getAllBoutique();
        $events = $db->getAllEvents();
        $articles = $db->getAllArticles();
        $mediatheques = $db->getAllMediatheque();
        break;
    case 'services':
        $services = $db->getAllServices();
        break;
    case 'groupes':
        $groupes = $db->getAllGroupes();
        break;
}

require('view.php');
