
<!-- template PHP Dynamique pour tous les articles -->
<article>
    <h3><?php echo $mag->getNom()?></h3>
    <p id="date"><?php echo $mag->getDate() ?></p>
    <?php if($mag->getMedia() !== "") { ?>
        <iframe 
            width="560" 
            height="315" 
            src="<?php echo $mag->getMedia(); ?>" 
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen>
        </iframe>
    <?php } ?>
    <p><?php echo $mag->getDescription()?></p>
</article>