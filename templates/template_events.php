<article class="col-5" id="template_events">
        <h3><?php echo $event->getNom() ?></h3>
        <p id="date"><?php echo $event->getDate() ?></p>
        <div><span><?php echo $event->getLocalisation()?></span></div>
        <div><span><?php echo $event->getOrganisateur()?></span></div>
        <p><?php echo $event->getDescription() ?></p>
        <button id="adAgenda">Ajouter à l'agenda</button>
</article>