<?php
function jsonSerialize($article)
{
    return $article->toArray();
}

$data = [];

foreach ($boutique as $article) {
    array_push($data, jsonSerialize($article));
}

?>


<section id="page-boutique">
    <?php require("templates/template_navbar.php"); ?>
    <h1 id="title_page">La boutique</h1>
    <section class="buttons">
        <div class="col-8 container">
            <button class="btn active">Livres</button>
            <button class="btn">Magazines</button>
            <button class="btn">CD</button>
            <button class="btn">DVD</button>
            <button class="btn">Goodies</button>
        </div>
    </section>
    <section class="articles_boutique" id="boutique">

    </section>
</section>



<?php require("templates/template_boutique.html"); ?>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        let m = {
            category: "Livres",
            boutique: undefined,
            items: [],

            init: function() {
                this.boutique = <?php echo json_encode($data); ?>;
            }
        }

        let c = {
            init: function() {
                m.init();
                v.init();
            },

            filterCategories: function(e) {
                v.clearItems();
                v.renderItems(e.target.textContent);
            },

            setItemsByCategory: function() {

                m.items = m.boutique.filter(function(elem) {
                    if (m.category.toLowerCase() === elem.type.toLowerCase()) {
                        return elem;
                    }
                });

                v.renderItems();
            }
        }

        let v = {
            buttons: [],

            init: function() {
                this.initButtons();
                this.renderItems(c.setItemsByCategory());
            },

            renderItems: function() {
                let parentItems = document.querySelector(".articles_boutique");
                parentItems.innerHTML = "";
                m.items.forEach(function(elem) {
                    let content = document.getElementById("template_boutique").content;
                    let newItem = document.importNode(content, true);
                    newItem.querySelector(".article_boutique_container img").src = elem.image;
                    newItem.querySelector(".article_boutique_container h2").textContent = elem.titre;
                    newItem.querySelector(".article_boutique_container p").textContent = elem.description;
                    newItem.getElementById("prix_boutique").textContent = elem.prix + "€";
                    parentItems.appendChild(newItem);
                });
            },

            initButtons: function() {
                v.buttons = document.querySelectorAll('.buttons .btn');
                v.buttons.forEach(function(elem) {
                    elem.addEventListener('click', function(e) {
                        v.removeClasses();
                        this.classList.add('active');
                        m.category = e.target.innerHTML;
                        c.setItemsByCategory();
                    });
                });

                // let payBtns = document.querySelectorAll('.pay-btn');

                // payBtns.forEach(function(elem){
                //     elem.addEventListener('click', function(e){
                //         //TODO Traitement de paiement
                //     });
                // });
            },

            removeClasses: function() {
                v.buttons.forEach(function(elem, index) {
                    elem.classList.remove('active');
                })
            }
        }

        c.init();


    })
</script>